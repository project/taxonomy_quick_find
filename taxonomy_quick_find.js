
//Define the quick find function
Drupal.taxonomyQuickFind = function(delta, select) {
  //Get the parent block which contains the drop down menu
  var block = select.parentNode.parentNode.parentNode.parentNode;

  //Disable the select box
  select.disabled = true;

  //Set the semaphore for this block so we know when its safe to run the AJAX...
  block.blockActive = false;


  //Get the Unordered Lists within the block and slide them up.
  $('.item-list ul', block).animate(
    { height: 'hide', opacity: 'hide' },
    'slow',
    function() {
      //Once we've finished hiding once of the blocks (they should all take roughly the same ammount of time...
      //  ... we can run the AJAX

      if (!block.blockActive) {
        //Set the block as 'active'
        block.blockActive = true;

        //Do an JSON callback using the AJAX handler
        $.ajax({
          type: "GET",
          url: "/taxonomy_quick_find/" + delta + "/" + select.value,
          dataType: "json",
          success: function(msg){
            //On sucess, loop over the JS Array...
            for (x in msg) {
              //Each 'x' is a node type section, eg 'blog' or 'page'
              var linksForType = msg[x];

              //Grab the List within the block
              var ulList = $('.tqf_' + x, block);

              //Empty it
              $(ulList).empty();

              //Each 'y' is a node. Build a list item with a hyperlink in it linking to the url provided in the JS Array
              for (y in linksForType) {
                if (typeof(linksForType[y].url) == 'undefined') {
                  $('<li>' + linksForType[y].title + '</li>').appendTo(ulList);
                }
                else {
                  $('<li><a href="' + linksForType[y].url + '">' + linksForType[y].title + '</a></li>').appendTo(ulList);
                }
              }
            }

            //Now the new content is set, we can make the content appear again.
            if (block.blockActive) {
              $('.item-list ul', block).animate(
                { height: 'show', opacity: 'show' },
                'slow',
                function() {
                  block.blockActive = false;
                  select.disabled = false;
                }
              );
            }
          }
        });
      }
    }
  );
}
